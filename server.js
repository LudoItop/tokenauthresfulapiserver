var express = require('express');
var app = express();
var jwt = require('express-jwt');
var bodyParser = require('body-parser'); //bodyparser + json + urlencoder
var morgan  = require('morgan'); // logger
var tokenManager = require('./config/token_manager');
var secret = require('./config/secret');

app.listen(3001);
app.use(bodyParser());
app.use(morgan());

//Routes
var routes = {};
routes.courses = require('./route/courses.js');
routes.users = require('./route/users.js');


app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', 'http://localhost');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  if ('OPTIONS' == req.method) return res.send(200);
  next();
});

//Get all published courses
app.get('/courses', routes.courses.list);

//Get all courses
app.get('/courses/all', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.courses.listAll);

//Create a new course
app.post('/course', jwt({secret: secret.secretToken}), tokenManager.verifyToken , routes.courses.create);


//Get the post id
app.get('/course/:id', routes.courses.read);

//Create a new user
app.post('/user/register', routes.users.register); 

//Login
app.post('/user/signin', routes.users.signin);

//Logout
app.get('/user/logout', jwt({secret: secret.secretToken}), routes.users.logout);


console.log('Server API is starting on port 3001');