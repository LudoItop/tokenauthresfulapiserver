var db = require('../config/mongo_database.js');

var publicFields = '_id title url tags content created likes';

exports.list = function(req, res) {
	var query = db.courseModel.find({is_published: true});

	query.select(publicFields);
	query.sort('-created');
	query.exec(function(err, results) {
		if (err) {
  			console.log(err);
  			return res.send(400);
  		}

  		for (var courseKey in results) {
    		results[courseKey].content = results[courseKey].content.substr(0, 400);
    	}

  		return res.json(200, results);
	});
};

exports.listAll = function(req, res) {
	if (!req.user) {
		return res.send(401);
	}

	var query = db.courseModel.find();
	query.sort('-created');
	query.exec(function(err, results) {
		if (err) {
  			console.log(err);
  			return res.send(400);
  		}

  		for (var courseKey in results) {
    		results[courseKey].content = results[courseKey].content.substr(0, 400);
    	}

  		return res.json(200, results);
	});
};

exports.read = function(req, res) {
	var id = req.params.id || '';
	if (id == '') {
		return res.send(400);
	}

	var query = db.courseModel.findOne({_id: id});
	query.select(publicFields);
	query.exec(function(err, result) {
		if (err) {
  			console.log(err);
  			return res.send(400);
  		}

  		if (result != null) {
  			result.update({ $inc: { read: 1 } }, function(err, nbRows, raw) {
				return res.json(200, result);
			});
  		} else {
  			return res.send(400);
  		}
	});
};


exports.create = function(req, res) {
	if (!req.user) {
		return res.send(401);
	}

	var course = req.body.course;
	if (course == null || course.title == null || course.content == null) {
		return res.send(400);
	}

	var courseEntry = new db.courseModel();
	courseEntry.title = course.title;
	courseEntry.is_published = course.is_published;
	courseEntry.content = course.content;

	courseEntry.save(function(err) {
		if (err) {
			console.log(err);
			return res.send(400);
		}

		return res.send(200);
	});
}
